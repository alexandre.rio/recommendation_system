# -*- coding: utf-8 -*-

"""
Created on Wed Mar 10 12:47:39 2021

@author: matthieufuteral-peter
"""

import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.sparse as sparse
import pdb


class simulated_data:
    def __init__(self, n_ind=100, n_item=10000, p=.99, n_groups=5, n_type=10, seed=42):
        """
        Simulate data matrix completion : each individual grades items from 1 to 5.
        Three main objects :
        M -> complete matrix
        M_masked -> matrix with only 100(1 - p)% observed values
        M_longformat -> (n_obs, 3) array with rows, columns and values of observed values

        :param n_ind: number of individuals (default: 100)
        :param n_item: number of items (default: 10,000)
        :param p: proportion of missing value (default: 0.99)
        :param n_groups: number of groups of people who like the same kind of items, low rank hypothesis (default: 5)
        :param n_type: number of type of items, e.g. 10 types means there are ten categories of items and items
                       in a same category are similar
        """

        self.n_ind = n_ind
        self.n_item = n_item
        self.p = p
        self.n_groups = n_groups
        self.n_type = n_type
        np.random.seed(seed)

        # Build incomplete matrix
        self.build()

        # Build long format dataset
        self.long_format()

    def build(self):
        """
        Build M and M_masked as described in init.
        :return: None
        """
        self.M = np.zeros((self.n_ind, self.n_item))

        self.probs = [[.65, .2, .1, 0.03, 0.02],
                      [2.5/30, 5/30, 15/30, 5/30, 2.5/30],
                      [0.02, 0.03, 0.1, .2, .65]]

        cluster_size = self.n_ind // self.n_groups
        type_size = self.n_item // self.n_type

        index_ind = np.array(range(self.n_ind))
        index_items = np.array(range(self.n_item))
        np.random.shuffle(index_ind)
        np.random.shuffle(index_items)

        for i in range(self.n_groups):
            ind_idx = index_ind[i*cluster_size: (i+1)*cluster_size]
            probabilities = [self.probs[np.random.randint(len(self.probs))] for _ in range(self.n_type)]
            for j in range(self.n_type):
                item_idx = index_items[j*type_size: (j+1)*type_size]
                self.M[np.ix_(ind_idx, item_idx)] = np.random.multinomial(n=1, pvals=probabilities[j],
                                                                     size=(len(ind_idx), len(item_idx))).argmax(axis=-1) + 1

        mask_probs = np.random.binomial(n=1, p=1-self.p, size=self.M.shape)
        self.M_masked = self.M.copy() * mask_probs
        self.M_test = self.M.copy() * (1 - mask_probs)

    def long_format(self):
        """
        Compute long format dataset : first column is row index, second is column index, third is value
        :return: None
        """
        i, j = np.where(self.M_masked != 0)
        v = self.M_masked[self.M_masked != 0]
        self.M_longformat = np.column_stack([i, j, v])

    def compute_rank(self):
        """
        Compute rank of M
        :return: rank of M
        """
        return np.linalg.matrix_rank(self.M)


class data_pipeline:
    def __init__(self, p=.8):
        """
        pipeline to format movielens dataset to build Longformat matrix, sparse matrix and train / test matrices.
        :param p: % of training data
        """
        self.path_data = os.path.join(os.getcwd(), "data", "ml-100k", "u.data")
        self.p = p
        self.load()
        np.random.seed(42)

    def load(self):
        """
        Generate Longformat data (M_longformat) and masked sparse matrix (M_masked)
        Long format : (n_sample, (user_id, movie_id, rating))
        """
        self.M_longformat = pd.read_table(self.path_data,
                                          sep=r"\t",
                                          names=["user_id", "movie_id", "rating", "timestamp"]).values[:, :-1]
        self.n_users = np.unique(self.M_longformat[:, 0])[-1]
        self.n_movies = np.unique(self.M_longformat[:, 1])[-1]
        self.M_masked = sparse.csr_matrix((self.M_longformat[:, -1], (self.M_longformat[:, 0] - 1,
                                                                      self.M_longformat[:, 1] - 1)),
                                   shape=(self.n_users, self.n_movies)).toarray()

    def to_implicit_feedback(self):
        """
        Convert the original explicit feedback data (M_longformat) to implicit feedback data
        :return:
        """
        self.M_longformat[:, -1] = 1

    def build_train(self):
        """
        Build training and test dataset in long format (train & test) and sparse training
        (M_train)
        """

        self.user_ids = np.arange(self.n_users)
        np.random.shuffle(self.user_ids)

        self.user_train = np.sort(self.user_ids[:int(self.p * self.n_users)])
        self.user_test = np.sort(self.user_ids[int(self.p * self.n_users):])

        np.random.shuffle(self.M_longformat)
        n_sample = len(self.M_longformat)
        self.train = self.M_longformat[:int(self.p * n_sample)]
        self.test = self.M_longformat[int(self.p * n_sample):]

        self.M_train = sparse.csr_matrix((self.train[:, -1], (self.train[:, 0] - 1, self.train[:, 1] - 1)),
                                         shape=(self.n_users, self.n_movies)).toarray()
        self.M_test = sparse.csr_matrix((self.test[:, -1], (self.test[:, 0] - 1, self.test[:, 1] - 1)),
                                         shape=(self.n_users, self.n_movies)).toarray()


if __name__ == "__main__":

    # Check low rank matrix with default parameters
    data = simulated_data()
    U, s, V = np.linalg.svd(data.M)

    # Display singular values
    plt.plot(s)
    plt.title("Singular values")
    plt.show()





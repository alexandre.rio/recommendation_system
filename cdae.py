# -*- coding: utf-8 -*-

"""
Created on Mon Mar 15 16:31:00 2021

@author: alexandre-rio
"""

import argparse
import os
os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'
import numpy as np
from tqdm import tqdm

import torch.nn as nn
from torch.utils.data import DataLoader
from torch.utils.data import TensorDataset
import torch.nn.functional as F
import torch
import pdb
from uuid import uuid4

from util import simulated_data, data_pipeline


class CDAE(nn.Module):
    def __init__(self, n_users, context_dim, latent_dim=256):
        super(CDAE, self).__init__()

        self.n_users = n_users
        self.context_dim = context_dim
        self.latent_dim = latent_dim

        self.user_embedding = nn.Embedding(self.n_users, self.latent_dim)
        self.dense_1 = nn.Linear(self.context_dim, self.latent_dim)
        self.dense_2 = nn.Linear(self.latent_dim, self.context_dim)
        self.sigmoid = nn.Sigmoid()

    def forward(self, r_corrup, user_ids):

        # Encode
        embedding = self.user_embedding(user_ids)
        enc = self.sigmoid(self.dense_1(r_corrup) + embedding)

        # Reconstruct
        rec = self.sigmoid(self.dense_2(enc))

        return rec


def train(data_loader, model, device, params):
    """
    Train CDAE
    """

    global_loss = []
    model.to(device)

    # Instantiate loss and optimizer with L2-regularization
    mse_loss = nn.MSELoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=params.learning_rate, weight_decay=params.l2_penalty)

    for epoch in range(params.n_epochs):

        epoch_loss = []
        # Train generator
        for b, (train_data, users_ids) in tqdm(enumerate(data_loader, 1)):

            train_data = train_data.to(device)
            users_ids = users_ids.to(device)

            # Binarize and corrupt purchase vectors
            r = torch.where(train_data.clone() != 0, 1., 0.)
            mask = torch.bernoulli(torch.ones_like(r) * (1 - params.dropout_ratio))
            r_corrup = r.clone() * mask.clone()

            optimizer.zero_grad()
            # Apply auto-encoder and output reconstructed vectors
            rec = model(r_corrup, users_ids)
            # Compute and store MSE loss
            loss = mse_loss(rec, r)
            epoch_loss.append(loss.data.item())

            loss.backward()
            # Take an optimization step
            optimizer.step()

        mean_loss_epoch = float(np.mean(epoch_loss))

        global_loss.append(mean_loss_epoch)
        if params.display:
            print(
                "[Epoch %d/%d] [Loss: %f]" % (epoch + 1, params.n_epochs, mean_loss_epoch)
            )

    return model, global_loss


def test(data_loader, model, device, params):

    test_loss = []
    model.to(device)
    mse_loss = nn.MSELoss()

    for b, (test_data, users_ids) in tqdm(enumerate(data_loader, 1)):

        test_data = test_data.to(device)
        users_ids = users_ids.to(device)

        # Binarize and corrupt purchase vectors
        r = torch.where(test_data.clone() != 0, 1., 0.)
        mask = torch.bernoulli(torch.ones_like(r) * (1 - params.dropout_ratio))
        r_corrup = r.clone() * mask.clone()

        with torch.no_grad():
            rec = model(r_corrup, users_ids)
            loss = mse_loss(rec, r)
            test_loss.append(loss.data.item())

    print(
        "Loss: %f" % (np.mean(test_loss))
    )

    pass


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    # General parameters
    parser.add_argument("--seed", type=int, default=0, help="seed")
    parser.add_argument("--display", type=bool, default=False, help="true to display training results")

    # Model parameters
    parser.add_argument("--dropout_ratio", type=float, default=.2, help="proportion of dropped out entries")
    parser.add_argument("--l2_penalty", type=float, default=.01, help="Scaling factor for L2 regularization")

    # Training parameters
    parser.add_argument("--n_epochs", type=int, default=100, help="batch size for the generator")
    parser.add_argument("--batch_size", type=int, default=32, help="batch size for the generator")
    parser.add_argument("--learning_rate", type=float, default=1e-3, help="learning rate for the generator")
    parser.add_argument("--validate_every", type=int, default=1, help="validate every x epochs")

    # Build parser
    params = parser.parse_args()

    np.random.seed(params.seed)  # Set for data shuffling

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # Makedir Models, Results and generate RUN id
    os.makedirs("models", exist_ok=True)
    os.makedirs("results_gan", exist_ok=True)
    RUN_ID = str(uuid4())[0:4]

    # Load MovieLens data

    data = data_pipeline()
    data.build_train()
    n_users, context_dim = data.M_train.shape

    train_set = TensorDataset(torch.Tensor(data.M_train), torch.arange(n_users))
    train_loader = DataLoader(train_set,
                              batch_size=params.batch_size,
                              shuffle=True,
                              num_workers=0)

    test_set = TensorDataset(torch.Tensor(data.M_test), torch.arange(n_users))
    test_loader = DataLoader(test_set,
                             batch_size=params.batch_size,
                             shuffle=False,
                             num_workers=0)

    # Instantiate and train model
    cdae = CDAE(n_users, context_dim)
    cdae, global_loss = train(train_loader, cdae, device, params)

    # Save models and losses
    torch.save(cdae.state_dict(), f"models/cdae_{RUN_ID}.pt")
    np.save(f"results_gan/cdae_loss_{RUN_ID}.npy", np.array(global_loss))

    # Test
    test(test_loader, cdae, device, params)


    end=True
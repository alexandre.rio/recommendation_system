# -*- coding: utf-8 -*-

"""
Created on Mon Mar 15 16:31:00 2021

@author: alexandre-rio
"""
import argparse
import os
import numpy as np
import math
from tqdm import tqdm

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import TensorDataset, DataLoader

from util.data import data_pipeline
from cdae import CDAE
from uuid import uuid4


class Generator(nn.Module):
    def __init__(self, vector_dim):
        super(Generator, self).__init__()

        self.vector_dim = vector_dim

        def model_layers(in_feat, hidden_feat, out_feat):
            layers = [nn.Linear(in_feat, hidden_feat)]
            for _ in range(params.g_hidden_layers - 1):
                layers.append(nn.Linear(hidden_feat, hidden_feat))
                layers.append(nn.LeakyReLU())
            layers.append(nn.Linear(hidden_feat, out_feat))
            layers.append(nn.Sigmoid())
            return layers

        mod_layers = [nn.Linear(self.vector_dim, 1024),
                      nn.LeakyReLU(),
                      nn.Linear(1024, 512),
                      nn.LeakyReLU(),
                      nn.Linear(512, 128),
                      nn.LeakyReLU(),
                      nn.Linear(128, 512),
                      nn.LeakyReLU(),
                      nn.Linear(512, 1024),
                      nn.LeakyReLU(),
                      nn.Linear(1024, self.vector_dim),
                      nn.Sigmoid()]

        self.model = nn.Sequential(
            *mod_layers  # model_layers(self.vector_dim, params.g_hidden_nodes, self.vector_dim)
        )

    def forward(self, context):
        """
        :param context: user's or item's true purchase vector (or batch of purchase vectors)
        """
        x = self.model(context)
        return x

    def loss(self, r_fake, context, discriminator):
        '''
        Compute CFGAN_ZP loss
        :param r_gen: user's or item's purchase vector generated by the generator
        :param context: user's or item's true purchase vector
        :param k: binary vector indicating whether index j is in N^PM (k_j=1) or not (k_j=0)
        :param discriminator: the discriminator
        '''
        # Compute BCE loss 1-log(D(G(context))
        bce_loss = nn.BCELoss()

        mask = torch.where(context > 0, 1., 0.)
        r_masked = r_fake * mask

        predictions = discriminator(r_masked)

        labels = torch.ones_like(predictions)

        loss = bce_loss(predictions, labels)

        return loss


class Discriminator(nn.Module):
    def __init__(self, vector_dim):
        super(Discriminator, self).__init__()

        self.vector_dim = vector_dim

        def model_layers(in_feat, hidden_feat):
            layers = [nn.Linear(in_feat, hidden_feat), nn.LeakyReLU()]
            for _ in range(params.d_hidden_layers - 1):
                layers.append(nn.Linear(hidden_feat, hidden_feat))
                layers.append(nn.LeakyReLU())
            layers.append(nn.Linear(hidden_feat, 1))
            layers.append(nn.Sigmoid())
            return layers

        mod_layers = [nn.Linear(self.vector_dim, 256),
                      nn.LeakyReLU(),
                      nn.Linear(256, 1),
                      nn.LeakyReLU(),
                      nn.Sigmoid()]

        self.model = nn.Sequential(
            *mod_layers  # model_layers(self.vector_dim, params.d_hidden_nodes)
        )

    def forward(self, x):
        x = self.model(x)
        return x

    def loss(self, r_real, r_fake):
        bce_loss = nn.BCELoss()

        # Compute loss on real data
        predictions_real = self(r_real)
        real_labels = torch.ones_like(predictions_real)
        loss_real = bce_loss(predictions_real, real_labels)

        # Compute loss on fake data
        mask = torch.where(r_real > 0, 1, 0)
        predictions_fake = self(r_fake * mask)
        fake_labels = torch.zeros_like(predictions_fake)

        loss_fake = bce_loss(predictions_fake, fake_labels)

        return (loss_real + loss_fake) / 2


def compute_negative_items(data, user_ids, cdae, device, params):
    """
    Compute the negative items set and impute a value (0, 1 or 2) for a certain proportion of these inputs
    """
    n_users = data.shape[0]

    # Binarize data
    data_bin = torch.where(data != 0, 1., 0.).to(device)
    user_ids = user_ids.to(device)

    # Compute scores of interestingness with CDAE
    cdae.to(device)
    with torch.no_grad():
        scores = cdae(data_bin, user_ids)

    # ordered_scores = torch.argsort(scores, dim=1, descending=False)
    mask = torch.zeros_like(data)
    for user in range(n_users):
        # Get data and score vectors for the user
        data_bin_u, scores_u = data_bin[user].to('cpu'), scores[user].to('cpu')

        # Find items with no feedback
        zero_feedback_items = torch.where(data_bin_u == 0)[0]
        nb_items = int(params.prop_negative * zero_feedback_items.shape[0])

        # Select %S items with lowest interestingness score and give them a 1 value in the mask
        selected_items = zero_feedback_items[scores_u[zero_feedback_items].argsort()][:nb_items]
        mask[user, selected_items] = 1

    return mask


def train(data_loader, generator, discriminator, cdae, optimizer_G, optimizer_D, device, params, user_ids):
    """
    Train RAGAN
    """
    # Prepare data, models and training

    generator.to(device)
    discriminator.to(device)

    g_losses, d_losses = [], []

    for epoch in range(params.n_epochs):

        g_loss_epoch, d_loss_epoch = [], []

        for b, (train_data, users_ids) in tqdm(enumerate(data_loader, 1)):
            # Train generator

            users_ids = users_ids.to(device)

            # Compute negative item sets with CDAE
            neg_train = compute_negative_items(train_data, users_ids, cdae, device, params)
            # Impute negative items values based on these sets
            aug_train_data = train_data + params.delta * neg_train
            aug_train_data = aug_train_data.to(device)
            sample = aug_train_data.clone()

            # Scale the sample
            sample /= 5

            # Generate fake purchase vectors
            r_fake = generator(sample)
            # Compute and store generator loss
            g_loss = generator.loss(r_fake, sample, discriminator)

            # Reconstruction loss
            recons_loss = params.reconstruction_scaling * nn.MSELoss()(r_fake[aug_train_data > 0],
                                                                       aug_train_data[aug_train_data > 0])

            g_loss += recons_loss

            g_loss_epoch.append(g_loss.item())
            g_loss.backward(retain_graph=True)
            # Take an optimization step
            optimizer_G.zero_grad()
            optimizer_G.step()


            # Train Discriminator

            if b % params.d_update_every == 0:
                # Generate fake purchase vectors
                r_fake = generator(sample)

                optimizer_D.zero_grad()

                # Compute and store generator loss
                d_loss = discriminator.loss(sample, r_fake)
                d_loss_epoch.append(d_loss.item())

                d_loss.backward()
                # Take an optimization step
                optimizer_D.step()

        g_mean_loss_epoch = np.mean(g_loss_epoch)
        d_mean_loss_epoch = np.mean(d_loss_epoch)

        g_losses.append(g_mean_loss_epoch)
        d_losses.append(d_mean_loss_epoch)
        if params.display:
            print(
                "[Epoch %d/%d] [Generator loss: %f] [Discriminator loss: %f]" % (epoch + 1, params.n_epochs,
                                                                                float(g_mean_loss_epoch),
                                                                                float(d_mean_loss_epoch))
            )

    return generator, discriminator, g_losses, d_losses


def test(data_loader, generator, cdae, device, params):

    g_loss_epoch = []
    # Test generator
    for b, (test_data, train_data, users_ids) in tqdm(enumerate(data_loader, 1)):

        test_data, users_ids = test_data.to(device), users_ids.to(device)

        # Compute negative item sets with CDAE
        test_mask = torch.where(test_data != 0, 1., 0.).to(device)

        if test_data.max() == 0:
            continue

        neg_train = compute_negative_items(train_data, users_ids, cdae, device, params).to(device)
        # Impute negative items values based on these sets
        aug_train_data = test_data + params.delta * neg_train
        sample = aug_train_data.to(device)
        sample /= 5

        # Generate fake purchase vectors
        with torch.no_grad():
            r_fake = generator(sample)
            r_fake *= 5

        # Compute and store generator loss
        g_loss = torch.abs(r_fake[test_mask == 1] - test_data[test_mask == 1]).sum() / torch.sum(test_mask)

        g_loss_epoch.append(g_loss.item())

    print(
        "Test set [Generator MSE loss: %f]" % (float(np.mean(g_loss_epoch)))
    )

    pass


if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    # General parameters
    parser.add_argument("--seed", type=int, default=42, help="seed")
    parser.add_argument("--path_cdae_model", type=str, help="path to the trained CDAE model")
    parser.add_argument("--display", type=bool, default=False, help="true to display training results")

    # Network parameters
    parser.add_argument("--g_hidden_layers", type=int, default=1, help="number of hidden layers for the generator (>0)")
    parser.add_argument("--g_hidden_nodes", type=int, default=256, help="number of hidden nodes for the generator")
    parser.add_argument("--d_hidden_layers", type=int, default=1,
                        help="number of hidden layers for the discriminator (>0)")
    parser.add_argument("--d_hidden_nodes", type=int, default=256, help="number of hidden nodes for the discriminator")

    # Model parameters
    parser.add_argument("--delta", type=int, default=1, help="rating to impute to the negative items (0, 1 or 2)")
    parser.add_argument("--prop_negative", type=float, default=0.1,
                        help="proportion of negative items for which to impute"
                             "value")
    parser.add_argument("--prop_pm", type=float, default=0.1, help="proportion of negative items sampled for PM")
    parser.add_argument("--prop_zr", type=float, default=0.1, help="proportion of negative items sampled for ZR")
    parser.add_argument("--reconstruction_scaling", type=float, default=0.1,
                        help="scaling factor for the reconstruction term in the generator loss")

    # Training parameters
    parser.add_argument("--n_epochs", type=int, default=100, help="batch size for the generator")
    parser.add_argument("--g_batch_size", type=int, default=32, help="batch size for the generator")
    parser.add_argument("--g_learning_rate", type=float, default=1e-3, help="learning rate for the generator")
    parser.add_argument("--d_batch_size", type=int, default=32, help="batch size for the discriminator")
    parser.add_argument("--d_learning_rate", type=float, default=1e-3, help="learning rate for the discriminator")
    parser.add_argument("--d_update_every", type=int, default=1, help="update d very x iterations")

    # Build parser
    params = parser.parse_args()

    # Makedir Models and generate RUN id
    os.makedirs("models", exist_ok=True)
    os.makedirs("results_gan", exist_ok=True)
    RUN_ID = str(uuid4())[0:4]

    np.random.seed(params.seed)  # Set for data shuffling

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # Load MovieLens data

    data = data_pipeline()
    data.build_train()
    n_users, context_dim = data.M_train.shape
    user_ids = data.user_ids

    train_set = TensorDataset(torch.Tensor(data.M_train), torch.arange(n_users))
    train_loader = DataLoader(train_set,
                              batch_size=params.g_batch_size,
                              shuffle=True,
                              num_workers=0)

    test_set = TensorDataset(torch.Tensor(data.M_test), torch.Tensor(data.M_train), torch.arange(n_users))
    test_loader = DataLoader(test_set,
                             batch_size=params.g_batch_size,
                             shuffle=False,
                             num_workers=0)

    # Load a trained CDAE
    cdae = CDAE(n_users, context_dim)
    if params.path_cdae_model is not None:
        cdae.load_state_dict(torch.load(os.path.join("models", params.path_cdae_model)))

    # Instantiate models and optimizers
    generator, discriminator = Generator(context_dim), Discriminator(context_dim)
    optimizer_G = torch.optim.Adam(generator.parameters(), lr=params.g_learning_rate)
    optimizer_D = torch.optim.Adam(discriminator.parameters(), lr=params.d_learning_rate)

    # Train model
    generator, discriminator, g_losses, d_losses = train(train_loader,
                                                         generator,
                                                         discriminator,
                                                         cdae,
                                                         optimizer_G,
                                                         optimizer_D,
                                                         device,
                                                         params,
                                                         user_ids)

    # Save models and losses
    torch.save(generator.state_dict(), f"models/ragan_g_{RUN_ID}.pt")
    torch.save(discriminator.state_dict(), f"models/ragan_d_{RUN_ID}.pt")
    np.save(f"results_gan/ragan_g_loss_{RUN_ID}.npy", g_losses)
    np.save(f"results_gan/ragan_d_loss_{RUN_ID}.npy", d_losses)

    # Test model
    test(test_loader, generator, cdae, device, params)

    end=True
# -*- coding: utf-8 -*-

"""
Created on Wed Mar 10 12:47:39 2021

@author: matthieufuteral-peter
"""

import os
from tqdm.auto import tqdm
import argparse
import numpy as np
from numpy.linalg import svd as svd
from util import simulated_data, data_pipeline


def spectral_soft_thresh(A, mu):
    U, s, V = svd(A)
    s_mu = (s - mu) * (s > mu)
    S_mu = np.zeros((U.shape[1], V.shape[0]))
    S_mu[:s.size, :s.size] = np.diag(s_mu)
    return np.dot(U, np.dot(S_mu, V))


def prox_grad_descent(M, mat_mask, M_masked, M_test, mu, n_iter=100, verbose=False):
    """
    Perform Proximal gradient descend

    :param M: Complete data matrix (useless)
    :param mat_mask: 0, 1 mask matrix => 1 is observed, 0 unobserved
    :param M_masked: M * mat_mask => matrix with unobserved values
    :param mu: regularization param
    :param n_iter: number of iterations
    :param verbose: show progress or not
    :return: Complete matrix, list of errors
    """
    list_train_errors = []
    A = np.zeros(M_masked.shape)
    for i in tqdm(range(n_iter + 1)):
        A = spectral_soft_thresh(A * (1 - mat_mask) + M_masked, mu)
        train_err = test(A, M_masked)
        list_train_errors.append(train_err)
        if i % 100 == 0 and verbose == True:
            test_err = test(A, M_test)
            print("iteration = {}; test error = {}".format(i, test_err))
    return A, list_train_errors


def test(A, M_test):
    B = np.round(A.copy())
    B = np.where(B <= 1, 1, B)
    B = np.where(B >= 5, 5, B)
    mask = np.where(M_test == 0, 0, 1)
    res = np.abs(B - M_test)
    mae = np.sum(res[mask == 1]) / np.sum(mask)
    return mae


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('--p', type=float, default=.8)
    parser.add_argument('--mu', type=float, default=10)
    parser.add_argument('--n_iter', type=int, default=100)
    parser.add_argument('--simulated', action="store_true")
    parser.add_argument('--n_ind', type=int, default=100)
    parser.add_argument('--n_item', type=int, default=1000)
    parser.add_argument('--path_complete', type=str)
    args = parser.parse_args()

    if args.simulated:
        data = simulated_data(n_ind=args.n_ind, n_item=args.n_item, p=.9)
        M = data.M
        M_masked = data.M_masked
        mat_mask = np.where(M_masked == 0, 0, 1)
        M_longformat = data.M_longformat
        M_test = data.M_test

    else:
        data = data_pipeline(p=args.p)
        data.build_train()
        M = data.M_masked
        M_masked = data.M_train
        M_test = data.M_test
        mat_mask = np.where(M_masked == 0, 0, 1)

    A, list_errors = prox_grad_descent(M, mat_mask, M_masked, M_test, mu=args.mu, n_iter=args.n_iter, verbose=True)

    A = np.round(A)
    A = np.where(A <= 1, 1, A)
    A = np.where(A >= 5, 5, A)

    mae = test(A, M_test)
    print("Mean Absolute Error on test set : {:.3f}".format(mae))

    os.makedirs("results_cs", exist_ok=True)
    path_save = os.path.join("results_cs", args.path_complete)
    np.save(path_save, A)






# -*- coding: utf-8 -*-

"""
Created on Mon Mar 15 16:31:00 2021

@author: alexandre-rio
"""
import argparse
import os
import numpy as np
import math
from tqdm import tqdm
from uuid import uuid4

import torch.nn as nn
import torch.nn.functional as F
import torch

from util.data import data_pipeline
from torch.utils.data import TensorDataset, DataLoader


class Generator(nn.Module):
    def __init__(self, vector_dim):
        super(Generator, self).__init__()

        self.vector_dim = vector_dim

        def model_layers(in_feat, hidden_feat, out_feat):
            layers = [nn.Linear(in_feat, hidden_feat), nn.Sigmoid()]
            for _ in range(params.g_hidden_layers - 1):
                layers.append(nn.Linear(hidden_feat, hidden_feat))
                layers.append(nn.Sigmoid())
            layers.append(nn.Linear(hidden_feat, out_feat))
            layers.append(nn.Sigmoid())
            return layers

        self.model = nn.Sequential(
            *model_layers(self.vector_dim, params.g_hidden_nodes, self.vector_dim)
        )

    def forward(self, context):
        """
        :param context: user's or item's true purchase vector (or batch of purchase vectors)
        """
        x = self.model(context)
        return x

    def loss(self, r_fake, context, negative_zr, negative_pm, discriminator):
        '''
        Compute CFGAN_ZP loss
        :param r_gen: user's or item's purchase vector generated by the generator
        :param context: user's or item's true purchase vector
        :param k: binary vector indicating whether index j is in N^PM (k_j=1) or not (k_j=0)
        :param negative_zr: set of selected negative items for zero-reconstruction (array-like mask)
        :param negative_pm: set of selected negative items for partial-masking (array-like mask)
        :param discriminator: the discriminator
        '''
        # Compute BCE loss 1-log(D(G(context))
        bce_loss = nn.BCELoss(reduction='sum')

        mask = context + negative_pm
        r_masked = r_fake * mask

        predictions = discriminator(r_masked)
        labels = torch.zeros_like(predictions)

        loss = - bce_loss(predictions, labels)

        # Compute zero-regularization loss (MSE loss)
        mse_loss = nn.MSELoss(reduction='sum')

        r_zr = r_fake * negative_zr
        zr_target = torch.zeros_like(r_zr)

        reconstruction = mse_loss(r_zr, zr_target)

        return loss + params.reconstruction_scaling * reconstruction


class Discriminator(nn.Module):
    def __init__(self, vector_dim):
        super(Discriminator, self).__init__()

        self.vector_dim = vector_dim

        def model_layers(in_feat, hidden_feat):
            layers = [nn.Linear(in_feat, hidden_feat), nn.Sigmoid()]
            for _ in range(params.d_hidden_layers - 1):
                layers.append(nn.Linear(hidden_feat, hidden_feat))
                layers.append(nn.Sigmoid())
            layers.append(nn.Linear(hidden_feat, 1))
            layers.append(nn.Sigmoid())
            return layers

        self.model = nn.Sequential(
            *model_layers(self.vector_dim, params.d_hidden_nodes)
        )

    def forward(self, x):
        x = self.model(x)
        return x

    def loss(self, r_real, r_fake, negative_pm):
        bce_loss = nn.BCELoss()

        # Compute loss on real data
        predictions_real = self(r_real)
        real_labels = torch.ones_like(predictions_real)
        loss_real = bce_loss(predictions_real, real_labels)

        # Compute loss on fake data
        mask = r_real + negative_pm
        r_masked = r_fake * mask
        predictions_fake = self(r_masked)
        fake_labels = torch.zeros_like(predictions_fake)
        loss_fake = bce_loss(predictions_fake, fake_labels)

        return (loss_real + loss_fake) / 2

def compute_negative_items(data, params):
    """ Compute set of negative items for partial masking (PM) and zero reconstruction (ZR)"""
    zero_feedback_set = torch.where(data == 0)
    negative_pm, negative_zr = torch.zeros_like(data), torch.zeros_like(data)

    for user in range(data.shape[0]):
        user_zero_feedback_set = zero_feedback_set[1][zero_feedback_set[0] == user]
        nb_zero_items = user_zero_feedback_set.shape[0]
        negative_pm_set = np.random.choice(user_zero_feedback_set, size=int(params.prop_pm * nb_zero_items))
        negative_pm[user, negative_pm_set] = 1
        negative_zr_set = np.random.choice(user_zero_feedback_set, size=int(params.prop_zr * nb_zero_items))
        negative_zr[user, negative_zr_set] = 1

    return negative_pm, negative_zr

def train(data_loader, generator, discriminator, optimizer_G, optimizer_D, device, params):
    """
    Train CFGAN using Partial-Masking and Zero-Reconstruction
    """

    generator.to(device)
    discriminator.to(device)

    g_losses, d_losses = [], []

    for epoch in range(params.n_epochs):

        g_loss_epoch, d_loss_epoch = [], []

        for b, (train_data, users_ids) in tqdm(enumerate(data_loader, 1)):

            # Compute negative item sets for partial masking (PM) and zero reconstruction (ZR)
            negative_zr, negative_pm = compute_negative_items(train_data, params)
            negative_zr, negative_pm = negative_zr.to(device), negative_pm.to(device)

            # Train generator
            sample = train_data.to(device)

            optimizer_G.zero_grad()
            # Generate fake purchase vectors
            r_fake = generator(sample)
            # Compute and store generator loss
            g_loss = generator.loss(r_fake, sample, negative_zr, negative_pm, discriminator)
            g_loss_epoch.append(g_loss.item())
            g_loss.backward(retain_graph=True)
            # Take an optimization step
            optimizer_G.step()

            if b % params.d_update_every == 0:
                # Train discriminator
                optimizer_D.zero_grad()
                # Generate fake purchase vectors
                r_fake = generator(sample)
                # Compute and store generator loss
                d_loss = discriminator.loss(sample, r_fake, negative_pm)
                d_loss_epoch.append(d_loss.item())
                d_loss.backward()
                # Take an optimization step
                optimizer_D.step()

        g_mean_loss_epoch = np.mean(g_loss_epoch)
        d_mean_loss_epoch = np.mean(d_loss_epoch)

        g_losses.append(g_mean_loss_epoch)
        d_losses.append(d_mean_loss_epoch)
        if params.display:
            print(
                "[Epoch %d/%d] [Generator loss: %f] [Discriminator loss: %f]" % (epoch + 1, params.n_epochs,
                                                                                float(g_mean_loss_epoch),
                                                                                float(d_mean_loss_epoch))
            )

    return generator, discriminator, g_losses, d_losses


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    # General parameters
    parser.add_argument("--seed", type=int, default=0, help="seed")
    parser.add_argument("--display", type=bool, default=False, help="true to display training results")

    # Network parameters
    parser.add_argument("--g_hidden_layers", type=int, default=1, help="number of hidden layers for the generator (>0)")
    parser.add_argument("--g_hidden_nodes", type=int, default=50, help="number of hidden nodes for the generator")
    parser.add_argument("--d_hidden_layers", type=int, default=1,
                        help="number of hidden layers for the discriminator (>0)")
    parser.add_argument("--d_hidden_nodes", type=int, default=50, help="number of hidden nodes for the discriminator")

    # Model parameters
    parser.add_argument("--prop_pm", type=float, default=0.1, help="proportion of negative items sampled for PM")
    parser.add_argument("--prop_zr", type=float, default=0.1, help="proportion of negative items sampled for ZR")
    parser.add_argument("--reconstruction_scaling", type=float, default=0.1,
                        help="scaling factor for the reconstruction term in the generator loss")

    # Training parameters
    parser.add_argument("--n_epochs", type=int, default=1, help="batch size for the generator")
    parser.add_argument("--g_batch_size", type=int, default=32, help="batch size for the generator")
    parser.add_argument("--g_learning_rate", type=float, default=1e-3, help="learning rate for the generator")
    parser.add_argument("--d_batch_size", type=int, default=32, help="batch size for the discriminator")
    parser.add_argument("--d_learning_rate", type=float, default=1e-3, help="learning rate for the discriminator")
    parser.add_argument("--d_update_every", type=int, default=1, help="update d very x iterations")

    # Build parser
    params = parser.parse_args()

    # Makedir Models and generate RUN id
    os.makedirs("models", exist_ok=True)
    os.makedirs("results_gan", exist_ok=True)
    RUN_ID = str(uuid4())[0:4]

    np.random.seed(params.seed)  # Set for data shuffling

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # Load MovieLens data
    data = data_pipeline()
    data.to_implicit_feedback()
    data.build_train()
    n_users, context_dim = data.M_train.shape  # Size of the purchase vector

    train_set = TensorDataset(torch.Tensor(data.M_train), torch.arange(n_users))
    train_loader = DataLoader(train_set,
                              batch_size=params.g_batch_size,
                              shuffle=True,
                              num_workers=0)

    test_set = TensorDataset(torch.Tensor(data.M_test), torch.Tensor(data.M_train), torch.arange(n_users))
    test_loader = DataLoader(test_set,
                             batch_size=params.g_batch_size,
                             shuffle=False,
                             num_workers=0)

    # Instantiate model and optimizers
    generator, discriminator = Generator(context_dim), Discriminator(context_dim)
    optimizer_G = torch.optim.Adam(generator.parameters(), lr=params.g_learning_rate)
    optimizer_D = torch.optim.Adam(discriminator.parameters(), lr=params.d_learning_rate)

    # Train model
    generator, discriminator, g_losses, d_losses = train(train_loader,
                                                         generator,
                                                         discriminator,
                                                         optimizer_G,
                                                         optimizer_D,
                                                         device,
                                                         params)

    # Save models and losses
    torch.save(generator.state_dict(), f"models/cfgan_g_{RUN_ID}.pt")
    torch.save(discriminator.state_dict(), f"models/cfgan_d_{RUN_ID}.pt")
    np.save(f"results_gan/cfgan_g_loss_{RUN_ID}.npy", g_losses)
    np.save(f"results_gan/cfgan_d_loss_{RUN_ID}.npy", d_losses)


    end=True